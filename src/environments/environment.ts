// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'test-c8b72',
    appId: '1:626282293655:web:cac4fba3e172a8f0cafcd2',
    databaseURL: 'https://test-c8b72-default-rtdb.firebaseio.com',
    storageBucket: 'test-c8b72.appspot.com',
    apiKey: 'AIzaSyBrk5VoIkswrwQrewHTdHCZagPwnm3So8Q',
    authDomain: 'test-c8b72.firebaseapp.com',
    messagingSenderId: '626282293655',
    measurementId: 'G-4WRTN4TCK0',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
