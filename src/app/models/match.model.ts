export interface Matches {
  [key: string]: Match[]
}

export interface Match {
  teamA: string;
  teamB: string;
  timeMatch: string;
  winner: string;
  group: string,
  id: string,
  timeMatchRaw: string,
}