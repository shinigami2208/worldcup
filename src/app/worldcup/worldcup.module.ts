import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorldcupRoutingModule } from './worldcup-routing.module';
import { WorldcupComponent } from './worldcup.component';
import { HomeComponent } from './home/home.component';
import { RankingComponent } from './ranking/ranking.component';
import { SharedModule } from '../shared/shared.module';
import { MatchesItemComponent } from './home/matches-item/matches-item.component';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';


@NgModule({
  declarations: [
    WorldcupComponent,
    HomeComponent,
    RankingComponent,
    MatchesItemComponent
  ],
  imports: [
    CommonModule,
    WorldcupRoutingModule,
    SharedModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
  ]
})
export class WorldcupModule { }
