import { VariableBinding } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ResultService } from 'src/app/services/result.service';
import { UserService } from 'src/app/services/user.service';
import { VotingService } from 'src/app/services/voting.service';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss'],
})
export class RankingComponent {
  voteData: any = [];
  result: any = [];
  users: any = [];

  constructor(
    private resultService: ResultService,
    private votingService: VotingService,
    private userService: UserService
  ) {
    this.resultService.getResultsList().subscribe((res) => {
      this.result = res.map((e: any) => {
        const data = e.payload.doc.data();
        data.id = e.payload.doc.id;
        return data;
      });

      this.votingService.getVotingList().subscribe((res) => {
        this.voteData = res.map((e: any) => {
          const data = e.payload.doc.data();
          data.id = e.payload.doc.id;
          return data;
        });
        this.userService.getUsersList().subscribe((res) => {
          this.users = res.map((e: any) => {
            const data = e.payload.doc.data();
            data.id = e.payload.doc.id;
            return data;
          });
          this.getRanking();
        });
      });
    });
  }

  getRanking() {
    for (let i = 0; i < this.voteData.length; i++) {
      const matchResult = this.result.find((item: any) => item.id === this.voteData[i].matchId)?.result;

      const winners = this.voteData[i].vote.filter(
        (item: any) => item.vote === matchResult
      );

      const numberOfLosers = this.users.length - winners.length;

      if (winners.length !== this.users.length && winners.length !== 0) {
        const goldForWinner = Math.floor((numberOfLosers * 10) / winners.length);
        console.log(goldForWinner);

        this.users = this.users.map((user: any) => {
          const existWinner = winners.findIndex(
            (winner: any) => winner.uid === user.uid
          );

          if (existWinner === -1) {
            return { ...user, gold: user.gold - 10 };
          } else {
            return { ...user, gold: user.gold + goldForWinner };
          }
        });
      }
    }

    this.users = this.users.sort((item1: any, item2: any) => {
      if (item1.gold > item2.gold) {
        return -1;
      } else {
        return 1;
      }
    });
  }
}
