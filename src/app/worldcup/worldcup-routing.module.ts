import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RankingComponent } from './ranking/ranking.component';
import { WorldcupComponent } from './worldcup.component';

const routes: Routes = [
  {
    path: '',
    component: WorldcupComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent,
      },
      {
        path: 'ranking',
        component: RankingComponent,
      },
    ],
  },
  {},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorldcupRoutingModule {}
