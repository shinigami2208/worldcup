import { Component, Input, OnInit } from '@angular/core';
import { Match } from 'src/app/models/match.model';
import * as moment from 'moment';
import { VotingService } from 'src/app/services/voting.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: '[app-matches-item]',
  templateUrl: './matches-item.component.html',
  styleUrls: ['./matches-item.component.scss'],
})
export class MatchesItemComponent implements OnInit {
  @Input() match!: Match;
  idVoteTimeout: any;
  isVoteTimeout = false;
  currentUser: any;
  voteList: any = [];
  isDisableVoteButton = false;
  chosenTeam: any;

  constructor(private votingService: VotingService) {}

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser')!);
    this.votingService.getVotingList().subscribe((res) => {
      this.voteList = res.map((e: any) => {
        const data = e.payload.doc.data();
        data.id = e.payload.doc.id;
        return data;
      });

      const existMatch = this.voteList.find(
        (item: any) => item.matchId === this.match?.id
      );
      if (existMatch) {
        const existUserVote = existMatch.vote.find(
          (item: any) => item.uid === this.currentUser?.user?.uid
        );

        if(existUserVote) {
          this.isDisableVoteButton = true
          this.chosenTeam = existUserVote.vote
        }
      }
    });
``
    if (this.match) {
      this.idVoteTimeout = setInterval(() => {
        this.setDisableGroupButton(this.match);
      }, 1000);
    }
  }

  ngOnChange() {
    this.setDisableGroupButton(this.match);
  }

  ngOnDestroy() {
    clearInterval(this.idVoteTimeout);
  }

  setDisableGroupButton(match: Match) {
    const timeMatch = moment(match.timeMatchRaw);
    const currentMoment = moment();
    const duration = moment.duration(timeMatch.diff(currentMoment));
    if (duration.asSeconds() <= 3600) {
      this.isVoteTimeout = true;
    }
    return this.isVoteTimeout
  }

  vote(action: number, matchId: string) {
    if(this.setDisableGroupButton(this.match)) {
      alert("Bo di ban oi")
      return;
    }

    const indexOfMatchId: number = this.voteList.findIndex(
      (item: any) => item.matchId === matchId
    );

    if (indexOfMatchId !== -1) {
      const uid = this.currentUser?.user?.uid;
      const indexOfUserVote: number = this.voteList[
        indexOfMatchId
      ].vote.findIndex((item: any) => item.uid === uid);
      if (indexOfUserVote !== -1) {
        alert('Chicken');
        return;
      } else {
        const newVote = {
          matchId: matchId,
          vote: [
            ...this.voteList[indexOfMatchId].vote,
            {
              uid: this.currentUser?.user?.uid,
              vote: action,
              displayName: this.currentUser?.user?.displayName,
            },
          ],
        };
        this.votingService.deleteVoting(this.voteList[indexOfMatchId].id);
        this.votingService.addVoting(newVote);
        alert('Vote Thanh Congggggggggg');
      }
    } else {
      this.votingService.addVoting({
        matchId: matchId,
        vote: [
          {
            uid: this.currentUser?.user?.uid,
            vote: action,
            displayName: this.currentUser?.user?.displayName,
          },
        ],
      });
      alert('Vote Thanh Congggggggggg');
    }
  }
}
