import { Matches } from './../../models/match.model';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment'
import { WorldcupService } from 'src/app/services/ worldcup.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  matchesData: Matches = {}

  constructor(private worldcupService: WorldcupService) {
    this.matchesData = this.worldcupService.getMatches()
  }

  ngOnInit(): void {

  }

  transformDate(date: string) {
    return moment(date, 'MMDDYYYY').format('ddd, DD/MMM')
  }
}
