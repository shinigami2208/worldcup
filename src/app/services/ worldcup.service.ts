import { Injectable } from '@angular/core';
import { Matches } from '../models/match.model';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root',
})
export class WorldcupService {
  data: Matches = {
    11202022: [
      {
        id: '1',
        teamA: 'Qatar',
        teamB: 'Ecuador',
        timeMatch: moment(
          new Date('2022-11-20T23:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'A',
        timeMatchRaw: '2022-11-20T23:00:00',
      },
    ],
    11212022: [
      {
        id: '2',
        teamA: 'Anh',
        teamB: 'Iran',
        timeMatch: moment(
          new Date('2022-11-21T20:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'B',
        timeMatchRaw: '2022-11-21T20:00:00',
      },
      {
        id: '3',
        teamA: 'Senegai',
        teamB: 'Hà Lan',
        timeMatch: moment(
          new Date('2022-11-21T23:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'A',
        timeMatchRaw: '2022-11-21T23:00:00',
      },
    ],
    11222022: [
      {
        id: '4',
        teamA: 'Hoa Kỳ',
        teamB: 'Wales',
        timeMatch: moment(
          new Date('2022-11-22T02:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'B',
        timeMatchRaw: '2022-11-22T02:00:00',
      },
      {
        id: '5',
        teamA: 'Argentina',
        teamB: 'Ả Rập Xê Út',
        timeMatch: moment(
          new Date('2022-11-22T17:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'C',
        timeMatchRaw: '2022-11-22T17:00:00',
      },
      {
        id: '6',
        teamA: 'Đan Mạch',
        teamB: 'Tunisia',
        timeMatch: moment(
          new Date('2022-11-22T20:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'D',
        timeMatchRaw: '2022-11-22T20:00:00',
      },
      {
        id: '7',
        teamA: 'Mexico',
        teamB: 'Ban Lan',
        timeMatch: moment(
          new Date('2022-11-22T23:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'C',
        timeMatchRaw: '2022-11-22T23:00:00',
      }
    ],
    11232022: [
      {
        id: '8',
        teamA: 'Pháp',
        teamB: 'Úc',
        timeMatch: moment(
          new Date('2022-11-23T02:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'D',
        timeMatchRaw: '2022-11-23T02:00:00',
      },
      {
        id: '9',
        teamA: 'Maroc',
        teamB: 'Croatia',
        timeMatch: moment(
          new Date('2022-11-23T17:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'F',
        timeMatchRaw: '2022-11-23T17:00:00',
      },
      {
        id: '10',
        teamA: 'Đức',
        teamB: 'Nhật Bản',
        timeMatch: moment(
          new Date('2022-11-23T20:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'E',
        timeMatchRaw: '2022-11-23T20:00:00',
      },
      {
        id: '11',
        teamA: 'Tây Ban Nha',
        teamB: 'Costa Rica',
        timeMatch: moment(
          new Date('2022-11-23T23:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'E',
        timeMatchRaw: '2022-11-23T23:00:00',
      }
    ],
    11242022: [
      {
        id: '12',
        teamA: 'Bỉ',
        teamB: 'Canada',
        timeMatch: moment(
          new Date('2022-11-24T02:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'F',
        timeMatchRaw: '2022-11-24T02:00:00',
      },
      {
        id: '13',
        teamA: 'Thuỵ Sĩ',
        teamB: 'Cameroon',
        timeMatch: moment(
          new Date('2022-11-24T17:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'G',
        timeMatchRaw: '2022-11-24T17:00:00',
      },
      {
        id: '14',
        teamA: 'Uruguay',
        teamB: 'Hàn Quốc',
        timeMatch: moment(
          new Date('2022-11-24T20:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'H',
        timeMatchRaw: '2022-11-24T20:00:00',
      },
      {
        id: '15',
        teamA: 'Bồ Đào Nha',
        teamB: 'Ghana',
        timeMatch: moment(
          new Date('2022-11-24T23:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'H',
        timeMatchRaw: '2022-11-24T23:00:00',
      }
    ],
    11252022: [
      {
        id: '16',
        teamA: 'Brasil',
        teamB: 'Serbia',
        timeMatch: moment(
          new Date('2022-11-25T02:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'G',
        timeMatchRaw: '2022-11-25T02:00:00',
      },
      {
        id: '17',
        teamA: 'Wales',
        teamB: 'Iran',
        timeMatch: moment(
          new Date('2022-11-25T17:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'B',
        timeMatchRaw: '2022-11-25T17:00:00',
      },
      {
        id: '18',
        teamA: 'Qatar',
        teamB: 'Senegai',
        timeMatch: moment(
          new Date('2022-11-25T20:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'A',
        timeMatchRaw: '2022-11-25T20:00:00',
      },
      {
        id: '19',
        teamA: 'Hà Lan',
        teamB: 'Ecuado',
        timeMatch: moment(
          new Date('2022-11-25T23:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'A',
        timeMatchRaw: '2022-11-25T23:00:00',
      }
    ],
    11262022: [
      {
        id: '20',
        teamA: 'Anh',
        teamB: 'Hoa Kỳ',
        timeMatch: moment(
          new Date('2022-11-26T02:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'B',
        timeMatchRaw: '2022-11-26T02:00:00',
      },
      {
        id: '21',
        teamA: 'Tunisia',
        teamB: 'Úc',
        timeMatch: moment(
          new Date('2022-11-26T17:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'D',
        timeMatchRaw: '2022-11-26T17:00:00',
      },
      {
        id: '22',
        teamA: 'Ba Lan',
        teamB: 'Ả Rập Xê Út',
        timeMatch: moment(
          new Date('2022-11-26T20:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'C',
        timeMatchRaw: '2022-11-26T20:00:00',
      },
      {
        id: '23',
        teamA: 'Pháp',
        teamB: 'Đan Mạch',
        timeMatch: moment(
          new Date('2022-11-26T23:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'D',
        timeMatchRaw: '2022-11-26T23:00:00',
      }
    ],
    11272022: [
      {
        id: '24',
        teamA: 'Argentina',
        teamB: 'Mexico',
        timeMatch: moment(
          new Date('2022-11-27T02:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'C',
        timeMatchRaw: '2022-11-27T02:00:00',
      },
      {
        id: '25',
        teamA: 'Nhật Bản',
        teamB: 'Costa Rica',
        timeMatch: moment(
          new Date('2022-11-27T17:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'E',
        timeMatchRaw: '2022-11-27T17:00:00',
      },
      {
        id: '26',
        teamA: 'Bỉ',
        teamB: 'Maroc',
        timeMatch: moment(
          new Date('2022-11-27T20:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'F',
        timeMatchRaw: '2022-11-27T20:00:00',
      },
      {
        id: '27',
        teamA: 'Croatia',
        teamB: 'Canada',
        timeMatch: moment(
          new Date('2022-11-27T23:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'F',
        timeMatchRaw: '2022-11-27T23:00:00',
      }
    ],
    11282022: [
      {
        id: '28',
        teamA: 'Tây Ban Nha',
        teamB: 'Đức',
        timeMatch: moment(
          new Date('2022-11-28T02:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'E',
        timeMatchRaw: '2022-11-28T02:00:00',
      },
      {
        id: '29',
        teamA: 'Cameroon',
        teamB: 'Serbia',
        timeMatch: moment(
          new Date('2022-11-28T17:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'G',
        timeMatchRaw: '2022-11-28T17:00:00',
      },
      {
        id: '30',
        teamA: 'Hàn Quốc',
        teamB: 'Ghana',
        timeMatch: moment(
          new Date('2022-11-28T20:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'H',
        timeMatchRaw: '2022-11-28T20:00:00',
      },
      {
        id: '31',
        teamA: 'Brazil',
        teamB: 'Thuỵ Sĩ',
        timeMatch: moment(
          new Date('2022-11-28T23:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'G',
        timeMatchRaw: '2022-11-28T23:00:00',
      }
    ],
    11292022: [
      {
        id: '32',
        teamA: 'Bồ Đào Nha',
        teamB: 'Uruguay',
        timeMatch: moment(
          new Date('2022-11-29T02:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'H',
        timeMatchRaw: '2022-11-29T02:00:00',
      },
      {
        id: '33',
        teamA: 'Ecuador',
        teamB: 'SenegaL',
        timeMatch: moment(
          new Date('2022-11-29T22:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'A',
        timeMatchRaw: '2022-11-29T22:00:00',
      },
      {
        id: '34',
        teamA: 'Hà Lan',
        teamB: 'Qatar',
        timeMatch: moment(
          new Date('2022-11-29T22:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'A',
        timeMatchRaw: '2022-11-29T22:00:00',
      }
    ],
    11302022: [
      {
        id: '36',
        teamA: 'Iran',
        teamB: 'Hoa Kỳ',
        timeMatch: moment(
          new Date('2022-11-30T02:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'B',
        timeMatchRaw: '2022-11-30T02:00:00',
      },
      {
        id: '37',
        teamA: 'Wales',
        teamB: 'Anh',
        timeMatch: moment(
          new Date('2022-11-30T02:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'B',
        timeMatchRaw: '2022-11-30T02:00:00',
      },
      {
        id: '38',
        teamA: 'Tunisia',
        teamB: 'Pháp',
        timeMatch: moment(
          new Date('2022-11-30T22:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'D',
        timeMatchRaw: '2022-11-30T22:00:00',
      },
      {
        id: '39',
        teamA: 'Úc',
        teamB: 'Đan Mạch',
        timeMatch: moment(
          new Date('2022-11-30T22:00:00').toLocaleString('en-US', {
            timeZone: 'Asia/Ho_Chi_Minh',
          })
        ).format('ddd/MMM, H:mm'),
        winner: '',
        group: 'D',
        timeMatchRaw: '2022-11-30T22:00:00',
      }
    ],
  };
  constructor() {}

  getMatches() {
    return this.data;
  }
}
