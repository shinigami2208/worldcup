import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';
import { GoogleAuthProvider } from 'firebase/auth';
import { Subject } from 'rxjs';
import {
  AngularFireList,
  AngularFireObject,
} from '@angular/fire/compat/database';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private currentUser: Subject<any> = new Subject();

  constructor(
    private firebaseAuth: AngularFireAuth,
    private router: Router,
    private userService: UserService
  ) {
    const user = JSON.parse(localStorage.getItem('currentUser')!)
    if(user) {
      this.currentUser.next(user);
    }
  }

  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('currentUser')!);
    return user !== null ? true : false;
  }

  getCurrentUser() {
    return this.currentUser;
  }

  googleSignIn() {
    return this.firebaseAuth.signInWithPopup(new GoogleAuthProvider()).then(
      (res) => {
        this.router.navigate(['/home']);
        localStorage.setItem('currentUser', JSON.stringify(res));

        if (res && res?.additionalUserInfo?.isNewUser) {
          this.userService.addUser({
            uid: res?.user?.uid || '',
            email: res?.user?.email || '',
            displayName: res?.user?.displayName || '',
            photoUrl: res?.user?.photoURL || '',
            gold: 640
          })
        }
      },
      (err) => {
        alert(err.message);
      }
    );
  }

  signOut() {
    this.firebaseAuth.signOut().then(() => {
      this.router.navigate(['/login']);
    });
  }
}
