import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class ResultService {
  constructor(
    private afs: AngularFirestore
  ) {}
  
  getResultsList() {
    return this.afs.collection('/Result').snapshotChanges();
  }
}
