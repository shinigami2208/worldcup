import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(
    private afs: AngularFirestore
  ) {}

  addUser(user: User) {
    return this.afs.collection('/Users').add(user)
  }
  
  getUsersList() {
    return this.afs.collection('/Users').snapshotChanges();
  }
}
