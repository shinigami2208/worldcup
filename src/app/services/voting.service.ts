import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class VotingService {
  constructor(
    private afs: AngularFirestore
  ) {}

  addVoting(vote: any) {
    return this.afs.collection('/Voting').add(vote)
  }
  
  getVotingList() {
    return this.afs.collection('/Voting').snapshotChanges();
  }

  deleteVoting(votingId: any) {
    return this.afs.doc(`/Voting/${votingId}`).delete()
  }
}
